<?php
namespace App\BITM\PHP_Soldiers\Phonebook;
use App\BITM\PHP_Soldiers\Utility\Utility;

class Phonebook {
    
    public $id ;
    public $name ;
    public $number;
    public $email;
    //public $created = "";
    ///public $modified = "";
    // public $created_by = "";
    // public $modified_by = "";
   public $deleted_at; //soft delete
    
    public function __construct($data = false){
        
        if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        
        $this->name = $data['name'];
       $this->number = $data['number'];
       $this->email = $data['email'];
       $this->deleted_at = $data['deleted_at'];
    }
     public function lists($data = array()) {

        $phone = array();

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database");

        $query = "SELECT * FROM `phonebooks` WHERE deleted_at IS NULL " ;

//        Utility::dd($query);
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {

            $phone [] = $row;
        }
        return $phone;
    }
    public function show($id=false){
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database.");

        $query = "SELECT * FROM `phonebooks` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_object($result);
        
        return $row;
    }
    
    public function index(){
        
        $phone = array();
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database.");

        $query = "SELECT * FROM `phonebooks`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $phone[] = $row;
        }
        return $phone;
    }
    
    
    public function store(){
       
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database.");
        
        $query = "INSERT INTO `miniproject`.`phonebooks` ( `name`,`number`,`email`) VALUES ( '".$this->name."','".$this->number."','".$this->email."')";
        
        $result = mysql_query($query);
               
        if($result){
            Utility::message("phoneBook name is added successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
    
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database.");

        $query = "DELETE FROM `miniproject`.`phonebooks` WHERE `phonebooks`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("phoneook title is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    
        public function update($id=false){
            
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database.");
        
        $query = "UPDATE `miniproject`.`phonebooks` SET `name` = '".$this->name."', `number` = '".$this->number." , `email` = '".$this->email."' WHERE `phonebooks`.`id` = ".$this->id;

        $result = mysql_query($query);
               
        if($result){
            Utility::message("Phonebook name edited successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
       
       public function trash($id = false){

        if (!$id) {

            Utility::message("No id is avaiable!sorry");
        }

        $this->id=$id;
        $this->deleted_at = time();

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database");

        $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = '" . $this->deleted_at . "' WHERE `phonebooks`.`id` =" . $id;

        $result = mysql_query($query);

        //var_dump($result);
        //die();

        if ($result) {

            Utility::message("Data trashed succesfully");
        } else {

            Utility::message("Cannot trash");
        }
        Utility::redirect("lists.php");
    }

    public function trashed() {

        $Phone = array();

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database");

        $query = "SELECT * FROM `phonebooks` WHERE deleted_at IS NOT NULL";
        $result = mysql_query($query);

        while ($row = mysql_fetch_object($result)) {

            $Phone [] = $row;
        }
        return $Phone;
    }

 public function recover($id = false) {

        if (!$id) {

            Utility::massage("No id is avaiable!sorry");
        }

        $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database");
        $lnk = mysql_select_db("miniproject") or die("Cannot select database");

        $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = NULL WHERE `phonebooks`.`id` =" . $id;

        $result = mysql_query($query);

        if ($result) {

            Utility::message("Data recovered succesfully");
        } else {

            Utility::massage("Cannot recovered");
        }
        Utility::redirect("lists.php");
    }

    public function multiplerecover($ids = array()) {

        if (is_array($ids) && count($ids) > 0) {

            $_ids = implode(',', $ids);

            $conn = mysql_connect("localhost", "root", "") or die("Cannot connect database");
            $lnk = mysql_select_db("miniproject") or die("Cannot select database");

            $query = "UPDATE `miniproject`.`phonebooks` SET `deleted_at` = NULL WHERE `phonebooks`.`id` IN ($_ids) ";

            $result = mysql_query($query);

            if ($result) {
                Utility::message("Multiple id is recovered successfully.");
            } else {
                Utility::message(" Cannot recover.");
            }

            Utility::redirect("lists.php");
        } else {
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('lists.php');
        }
    }
}
    ?>