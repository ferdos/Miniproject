
<?php

include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'MiniProject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');

use App\BITM\PHP_Soldiers\Phonebook\Phonebook;
use  App\BITM\PHP_Soldiers\Utility\Utility;


$Phonebooks = new Phonebook();
$Phone = $Phonebooks->show($_GET['id']);

//Utility::dd($book);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Create</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <form action="update.php" method="post">
            <fieldset>
                <legend>Edit Phonebook </legend>
                     <input  
                           type="hidden" 
                           name="id"
                           value="<?php echo $Phone->id;?>"
                           />
                <div>
                    <label>Enter User Name</label>
                    <input autofocus="autofocus" 
                    
                           placeholder="Enter user name" 
                           type="text" 
                           name="name"
                     
                           required="required"
                           value="<?php echo $Phone->name;?>"
                           />
                 </div>
                <div>
                    <label>Enter Number</label>
                    <input placeholder="Enter number" 
                           type="text" 
                           name="number"
                           required="required"
                      value="<?php echo $Phone->number;?>"
                           />
                </div>
                     <div>
                    <label>Enter Email</label>
                    <input placeholder="Enter email" 
                           type="text" 
                           name="email"
                           required="required"
                      value="<?php echo $Phone->email;?>"
                           />
                </div>                   
                    
                    
                     
                <button  type="submit">Save</button>
                <button  type="submit">Save & Add Again</button>
<!--                <input type="submit" value="Save" />-->
                <input type="reset" value="Reset" />
            </fieldset>
        </form> 
        <nav>
            <li><a href="index.php">Go to List</a></li>
            <li><a href="javascript:history.go(-1)">Back</a></li>
        </nav>
    </body>
</html>


