<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Miniproject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
       
 use App\BITM\PHP_Soldiers\Phonebook\Phonebook;
 use  App\BITM\PHP_Soldiers\Utility\Utility;
    $Phone = new Phonebook();
    $Phones= $Phone->index(); 
    ?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float:right;
                width:60%;
            }
            #message{
                background-color:green;
            }

        </style>
    </head>
    <body>
        <h1>Phone Book </h1>
        
        <div id="message">
            <?php echo Utility::message(); ?>            
        </div>
        
        <div><span>Search / Filter </span> 
          <span id="utility">Download as <a href="pdf.php">PDF</a> | 
            <a href="01simple-download-xlsx.php">XL</a>   <a href="create.php">Add New</a></span>
            <select>
                <option>10</option>
                <option>20</option>
                <option>30</option>
                <option>40</option>
                <option>50</option>
            </select>
        </div>
        <table border="1">
            <thead>
                <tr>
                    <th>Sl.</th>
                     <th>Name</th>
                    <th>Number</th>
                    <th>Email</th> 
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               <?php
               $slno =1;
               foreach($Phones as $Phone){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>                    
                    <td><a href="show.php?id=<?php echo $Phone->id;?>"><?php echo $Phone->name;?></a></td>
                    <td><?php echo $Phone->number;?></td>
                    <td><?php echo $Phone->email;?></td>
                    <td> <a href="show.php?id=<?php echo $Phone->id;?>">View</a>
                        | <a href="edit.php?id=<?php echo $Phone->id;?>">Edit</a> 
                        
                        | <a href="delete.php?id=<?php echo $Phone->id;?>" <button type="submit" class="delete">Delete</button>   </a>                                               
                        |  <a href="trash.php?id=<?php echo $Phone->id;?>">Trash </a>             
                     /Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>
            </tbody>
        </table>
        
        <div><span> prev  1 | 2 | 3 next </span></div>
        <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>
        <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    
    
    $('#message').hide(100);
        </script>
    </body>
</html>
