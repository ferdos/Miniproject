<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Miniproject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
    
     use App\BITM\PHP_Soldiers\Phonebook\Phonebook;
     use  App\BITM\PHP_Soldiers\Utility\Utility;
    
    $Phone = new Phonebook();
    $Phones= $Phone->trashed(); 
    ?>
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Phonebook</title>
        <link href="../../../Resource/css/bootstrap.min.css" rel="stylesheet">
<!--        <style>
            
            #utility{
                
                float: end;
                
            }
            
        </style>-->

    </head>
    <body>

        <h2>Trashed lists</h2>
        <br/>
        <br/>
        
        <label><?php echo Utility::message(); ?></label>             
        

        <form action="multiplerecover.php" method="post">
            <div><button type="submit">Recover Multiple</button> | <button type="button" id="deleteall">Delete Multiple</button></div>
            <table border='1' class="table table-condensed"> 

                <thead>
<!--            <div><a href="create.php">Add new</a> | <span id="utility">Download as PDF | XLs </span></div>-->
                    <tr>
                        <th><input type="checkbox" name="markall" id="markall" >Markall</th>
                        <th>SI.</th>
                        <th>Name</th>
                        <th>Number</th>
                        <th>Email</th>                                               
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody
                <?php
                if (count($Phones) > 0) {

                    $sino = 1;
                    foreach ($Phones as $Phone) {
                        ?>
                            <tr>
                                <td><input type="checkbox" class="mark" name="mark[]" value="<?php echo $Phone->id; ?>"></td>
                                <td><?php echo $sino; ?></td>                                
                                <td><?php echo $Phone->name; ?></td>
                                <td><?php echo $Phone->number; ?></td>
                                 <td><?php echo $Phone->email; ?></td>
                                
                                
                              

                            </tr>

                            <?php
                            $sino++;
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="14">No record is available.</td>
                        </tr> 
                        <?php
                    }
                    ?>
                </tbody>
                
            </table>
        </form>
        



            <!--        java script start here....-->
            <script src="https://code.jquery.com/jquery-2.1.4.min.js" type="text/javascript" ></script>

            <script>
                $(document).ready(function () {
                    $(".delete").bind('click', function (e) {

                        var deleteitem = confirm('Are you sure you want to delete it?');

                        if (!deleteitem) {

                            e.preventDefault();
                        }


                    });

                    $('#message').hide(5000);

                    $('#markall').bind('click', function () {
                        if ($('#markall').is(':checked')) {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = true;  //select all checkboxes with class "mark"               
                            });
                        } else {
                            $('.mark').each(function () { //loop through each checkbox
                                this.checked = false;  //select all checkboxes with class "mark"               
                            });
                        }
                    });

                    $('#deleteall').bind('click', function (e) {

                        var startDeleteProcess = false;
                        $('.mark').each(function () { //loop through each checkbox
                            if ($(this).is(':checked')) {
                                startDeleteProcess = true;
                            }
                        });

                        if (startDeleteProcess) {
                            var deleteItem = confirm("Are you sure you want to delete all Items??");
                            if (deleteItem) {
                                document.forms[0].action = 'deletemultiple.php';
                                document.forms[0].submit();
                            }
                        } else {
                            alert("Please select an item first");
                        }


                    });


                });

            </script>
 
 <li><a href="index.php">Go to List</a></li>
 <li><a href="javascript:history.go(-1)">Back</a></li>
    </body>
</html>
