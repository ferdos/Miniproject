<?php
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Miniproject'.DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'startup.php');
       
 use App\BITM\PHP_Soldiers\Phonebook\Phonebook;
 use  App\BITM\PHP_Soldiers\Utility\Utility;
    $phone = new Phonebook();
    $phones= $phone->index(); 
    
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');
    
require_once $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Miniproject' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'phpoffice' . DIRECTORY_SEPARATOR . 'phpexcel' . DIRECTORY_SEPARATOR . 'Classes' . DIRECTORY_SEPARATOR . 'PHPExcel.php';


$objPHPExcel = new PHPExcel();


$objPHPExcel->getProperties()->setCreator("PHP_Soldiers")
        ->setLastModifiedBy("PHP_Soldiers")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");


$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Name')
        ->setCellValue('B1', 'Number')       
        ->setCellValue('C1', 'Email');       
$counter = 2;
foreach ($phones as $phone) {
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $counter, $phone->name)
            ->setCellValue('B' . $counter, $phone->number)            
            ->setCellValue('C' . $counter, $phone->email);
                    
    $counter++;
}

$objPHPExcel->getActiveSheet()->setTitle('Simple');

$objPHPExcel->setActiveSheetIndex(0);


header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="01simple.xlsx"');
header('Cache-Control: max-age=0');

header('Cache-Control: max-age=1');


header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
