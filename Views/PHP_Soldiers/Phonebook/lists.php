<?php
include_once ($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'Miniproject' . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'startup.php');

use App\BITM\PHP_Soldiers\Phonebook\Phonebook;
use  App\BITM\PHP_Soldiers\Utility\Utility;


$Phone = new Phonebook($_POST);
$Phones = $Phone->lists();
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Phone Book</title>
        <style>
            
            #utility{
                
                background-color: greenyellow;
            }
            
        </style>
    </head>
    <body>

        <table border="1">
            <label><h2>Phone Book List</h2></label>
            <br/>
            <br/>
            <label><span id="utility" ><?php echo Utility::message(); ?></span></label>
            <br/>
            <label><span><a href="create.php">Add New</a>  <span id="utility">Download as <a href="pdf.php">PDF</a> | 
            <a href="01simple-download-xlsx.php">XL</a>| <a href="trashed.php">All Trashed</a></span></label>
            <tr>             
                <th>SI.</th>
                <th>Name</th>
                <th>Number</th>
                <th>Email</th>
                <th>Action</th>   
            </tr>
            <?php
               $slno =1;
               foreach($Phones as $Phone){
               ?>
                <tr>
                    <td><?php echo $slno;?></td>                    
                    <td><a href="show.php?id=<?php echo $Phone->id;?>"><?php echo $Phone->name;?></a></td>
                    <td><?php echo $Phone->number;?></td>
                    <td><?php echo $Phone->email;?></td>
                    <td> <a href="show.php?id=<?php echo $Phone->id;?>">View</a>
                        | <a href="edit.php?id=<?php echo $Phone->id;?>">Edit</a> 
                        
                        | <a href="delete.php?id=<?php echo $Phone->id;?>" <button type="submit" class="delete">Delete</button>   </a>                                               
                        |  <a href="trash.php?id=<?php echo $Phone->id;?>">Trash </a>             
                     | Recover | Email to Friend </td>
                </tr>
            <?php
           $slno++;
            }
            ?>

        </table>

    </body>
</html>
